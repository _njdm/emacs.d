;;; init.el --- my emacs config

;;; Commentary:

;; Installed packages:
;; - company
;; - relative-line-numbers
;; - color-identifiers-mode
;; - sr-speedbar
;; - flycheck
;; - c-eldoc
;; - magit
;; - multiple-cursors
;; - ruby-end
;; - smex
;; - haskell-mode
;; - ws-butler
;; - nhexl-mode
;; - org-present
;; - yasnippet
;; - impatient-mode
;; - htmlize
;; - simple-httpd
;; - emmet-mode
;; - company-c-headers
;; - rainbow-delimiters-mode
;; - pretty-mode
;; - web-mode
;; - php-mode
;; - php-auto-yasnippets
;; - rust-mode
;; - js2-mode
;; - pug-mode
;; - evil-mode

;; - yasnippet-latex

;;; Code:

;;; Emacs package manager
(require 'package)

;; Add melpa to package list
(add-to-list 'package-archives
             '("melpa"                   . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))

;; Initialize package manager
(package-initialize)

;;; evil-mode
(require 'evil)
(evil-mode 1)

;; smash esc
(require 'key-chord)
(key-chord-mode 1)
(key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
(key-chord-define evil-insert-state-map "kj" 'evil-normal-state)

;; ,w to save
(key-chord-define evil-normal-state-map ",w" 'save-buffer)
;; ,a to align
(key-chord-define evil-normal-state-map ",a" 'align)

;; esc quits
(defun my/minibuffer-keyboard-quit ()
  "Abort recursive edit.
In Delete Selection mode, if the mark is active, just deactivate it;
then it takes a second \\[keyboard-quit] to abort the minibuffer."
  (interactive)
  (if (and delete-selection-mode transient-mark-mode mark-active)
      (setq deactivate-mark  t)
    (when (get-buffer "*Completions*") (delete-windows-on "*Completions*"))
    (abort-recursive-edit)))

(define-key evil-normal-state-map [escape] 'keyboard-quit)
(define-key evil-visual-state-map [escape] 'keyboard-quit)
(define-key minibuffer-local-map [escape] 'my/minibuffer-keyboard-quit)
(define-key minibuffer-local-ns-map [escape] 'my/minibuffer-keyboard-quit)
(define-key minibuffer-local-completion-map [escape] 'my/minibuffer-keyboard-quit)
(define-key minibuffer-local-must-match-map [escape] 'my/minibuffer-keyboard-quit)
(define-key minibuffer-local-isearch-map [escape] 'my/minibuffer-keyboard-quit)

;; window movement
(define-key evil-normal-state-map (kbd "C-h") 'evil-window-left)
(define-key evil-normal-state-map (kbd "C-j") 'evil-window-down)
(define-key evil-normal-state-map (kbd "C-k") 'evil-window-up)
(define-key evil-normal-state-map (kbd "C-l") 'evil-window-right)

;;; Company mode
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(setq completion-ignore-case t)
(global-set-key (kbd "C-M-o") 'company-complete)

;;; Better C/C++ indent
(setq-default c-default-style "ellemtel")
(setq-default c-basic-offset 4)
(c-set-offset 'label '/)
(c-set-offset 'access-label '/)

;;; Show unnessecary whitespace
(add-hook 'prog-mode-hook (lambda ()
                            (interactive)
                            (setq show-trailing-whitespace t)))

;;; Use space for indentation
(setq-default indent-tabs-mode nil)

;;; Auto parens
(add-hook 'prog-mode-hook 'electric-pair-mode)

;;; Fuzzy file system (ido)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right .
 '(icomplete-mode t)
 '(ido-enable-flex-matching t)
 '(ido-mode (quote both) nil (ido))
 '(ido-use-virtual-buffers t)
 '(org-agenda-files
   (quote
    ("d:/Users/Niclas/Programmierung/Gamehacking/Library/Tasks.org" "~/org/school.org" "~/org/home.org"))))

;;; Shorter yes no dialog
(fset 'yes-or-no-p 'y-or-n-p)

;;; Enable gdb
;;(setq gdb-many-windows t)

;;; Faster glasses access
(define-key global-map (kbd "C-c g") 'glasses-mode)

;;; Auto semantics-mode
(add-hook 'prog-mode-hook #'semantic-mode)

;;; Faster align
(global-set-key (kbd "C-c C-k") 'align)

;;; Relative line numbers
(global-relative-line-numbers-mode)

;;; Turn on color-identifiers-mode
(add-hook 'prog-mode-hook #'color-identifiers-mode)

;;; Faster compiling
(global-set-key (kbd "C-c C-m") 'compile)

;;; Toggle speedbar
(global-set-key (kbd "C-c s") 'sr-speedbar-toggle)

;;; make .ipp a thing
(add-to-list 'auto-mode-alist '("\\.ipp\\'" . c++-mode))

;;; .h also c++
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;;; Jump between header and C/C++ file
(defvar my-cpp-other-file-alist
  '(("\\.cpp\\'" (".hpp" ".ipp"))
    ("\\.ipp\\'" (".hpp" ".cpp"))
    ("\\.hpp\\'" (".cpp" ".ipp"))
    ("\\.cxx\\'" (".hxx" ".ixx"))
    ("\\.ixx\\'" (".cxx" ".hxx"))
    ("\\.hxx\\'" (".cxx" ".ixx"))
    ("\\.c\\'" (".h"))
    ("\\.h\\'" (".c" ".c"))
    ))

(setq-default ff-other-file-alist 'my-cpp-other-file-alist)

(add-hook 'c-initialization-hook (lambda ()
                                   (define-key c-mode-base-map [(meta o)] 'ff-find-other-file)))

;;; Eldoc mode for elisp
(add-hook 'emacs-lisp-mode-hook 'eldoc-mode)

;;; Flycheck
(require 'flycheck)
(global-flycheck-mode)
(add-hook 'c++-mode-hook (lambda ()
                           (setq flycheck-gcc-language-standard "gnu++14")))

(add-hook 'c-mode-hook (lambda ()
                         (setq flycheck-gcc-language-standard "c11")))

;;; C-eldoc
(require 'c-eldoc)
(load "c-eldoc")
(add-hook 'c-mode-hook 'c-turn-on-eldoc-mode)

;;; org-mode
(require 'org)
(defun org-summery-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise.  N-DONE N-NOT-DONE."
  (let (org-log-done org-log-states)
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))

(add-hook 'org-after-todo-statistics-hook 'org-summary-todo)

;; Nicer org (indent)
(set-variable 'org-startup-indented t)

;; (syntax highlighting)
(setq org-src-fontify-natively t)

;; Add CLOSED: <TIMESTAMP> on DONE
(setq org-log-done 'time)

;; Global hotkeys
(global-set-key (kbd "C-c l") 'org-store-link)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c b") 'org-iswitchb)

;; Todo keywords
(setq org-todo-keywords
      '((sequence "TODO(t)" "|" "DONE(d!)" "CANCELED(c@)")))

;;; eval-and-replace
(defun my/eval-and-replace ()
  "Replace the preceding sexp with its value."
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
             (current-buffer))
    (error (message "Invalid expression")
           (insert (current-kill 0)))))

(global-set-key (kbd "C-c e") 'my/eval-and-replace)

;;; Magit
(global-set-key (kbd "C-x g") 'magit-status)

;;; Set compile-command per mode
(require 'compile)
;; C
(add-hook 'c-mode-hook (lambda ()
                         (set (make-local-variable 'compile-command)
                              "mingw32-make -k ")))

;; C++
(add-hook 'c++-mode-hook (lambda ()
                           (set (make-local-variable 'compile-command)
                                "mingw32-make -k ")))

;; Java
(add-hook 'java-mode-hook (lambda ()
                            (set (make-local-variable 'compile-command)
                                 (format "javac %s" (file-name-nondirectory buffer-file-name)))))

;; Rust
(add-hook 'rust-mode-hook (lambda ()
                            (set (make-local-variable 'compile-command)
                                 "cargo build")))

;;; Load brogrammer theme
(load "~/.emacs.d/lisp/brogrammer-theme.el")

;;; multiple-cursors
(global-set-key (kbd "C-S-SPC") 'set-rectangular-region-anchor)
(global-set-key (kbd "C-q") 'mc/mark-next-like-this)
(global-set-key (kbd "C-S-q") 'mc/mark-all-like-this)

;;; convinience function
(defun my/get-random-element-from-list (list)
  "Return random element from given LIST."
  (if (not (and (list) (listp list)))
      (nth (random (1- (1+ (length list)))) list)
    (error "Argument to get-random-element-from-list not a list or the list is empty")))

;;; smex
;;(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-x") '(lambda ()
                               (interactive)
                               (setq smex-prompt-string
                                     (my/get-random-element-from-list (list "M-x " "> " "Yes, Master? " "I love you " "Fuck you ")))
                               (smex)))
;; old M-x
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;;; ws-butler
(require 'ws-butler)
(add-hook 'prog-mode-hook 'ws-butler-mode)

;;; org-present
(eval-after-load "org-present"
  '(progn
     (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-display-inline-images)
                 (org-present-hide-cursor)
                 (org-present-read-only)))
     (add-hook 'org-present-mode-quit-hook
               (lambda ()
                 (org-present-small)
                 (org-remove-inline-images)
                 (org-present-show-cursor)
                 (org-present-read-write)))))

;;; yasnippet
(require 'yasnippet)
(yas-global-mode 1)
;; enable company completion
(defvar company-mode/enable-yas t
  "Enable yasnippet for all backends.")

(defun company-mode/backend-with-yas (backend)
  "Enables company mode with yasnippet snippets.  BACKEND."
  (if (or (not company-mode/enable-yas) (and (listp backend) (member 'company-yasnippet backend)))
      backend
    (append (if (consp backend) backend (list backend))
            '(:with company-yasnippet))))

(setq company-backends (mapcar #'company-mode/backend-with-yas company-backends))

(add-to-list 'company-backends 'company-c-headers)

;;; insert-line-before
(defun my/insert-line-before (times)
  "Insert TIMES newline(s) before the cursor."
  (interactive "p")
  (save-excursion
    (move-beginning-of-line 1)
    (newline times)))

(global-set-key (kbd "C-S-o") 'my/insert-line-before)

;;; emmet-mode
;; html-mode
(add-hook 'html-mode-hook 'emmet-mode)
(condition-case nil
    (define-key 'html-mode-map (kbd "C-j") 'emmet-expand-line)
  (error nil))

;; web-mode
(require 'web-mode)
(add-hook 'web-mode 'emmet-mode)
(define-key web-mode-map (kbd "C-j") 'emmet-expand-line)

;;; turn off tool-bar
(tool-bar-mode -1)

;;; column-number-mode
(column-number-mode 1)

;;; rainbow-delimiters-mode
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

;;; php-auto-yasnippets
(require 'php-auto-yasnippets)
(setq php-auto-yasnippet-php-program "~/.emacs.d/elpa/php-auto-yasnippets-20141128.1411/Create-PHP-YASnippet.php")

(define-key php-mode-map (kbd "C-c C-y") 'yas/create-php-snippet)
(define-key yas-minor-mode-map (kbd "C-c C-y") 'yas/create-php-snippet)
(add-hook 'web-mode-hook 'yas-minor-mode-map)

;;; switch between php-mode and html-mode
                                        ;(define-key php-mode-map (kbd "C-c C-z") 'html-mode)
                                        ;(condition-case nil
                                        ;    (define-key html-mode-map (kbd "C-c C-z") 'php-mode)
                                        ;  (error nil))

;;; web-mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))

;;; pretty-mode
(require 'pretty-mode)
(global-pretty-mode t)

;;; "modern" c++-mode
(require 'font-lock)

(defun --copy-face (new-face face)
  "Define NEW-FACE from existing FACE."
  (copy-face face new-face)
  (eval `(defvar ,new-face nil))
  (set new-face new-face))

(--copy-face 'font-lock-label-face  ; labels, case, public, private, proteced, namespace-tags
             'font-lock-keyword-face)
(--copy-face 'font-lock-doc-markup-face ; comment markups such as Javadoc-tags
             'font-lock-doc-face)
(--copy-face 'font-lock-doc-string-face ; comment markups
             'font-lock-comment-face)

(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)


(add-hook 'c++-mode-hook
          '(lambda()
             (font-lock-add-keywords
              nil '(;; complete some fundamental keywords
                  ("\\<\\(void\\|unsigned\\|signed\\|char\\|short\\|bool\\|int\\|long\\|float\\|double\\)\\>" . font-lock-keyword-face)
                  ;; add the new C++11 keywords
                  ("\\<\\(alignof\\|alignas\\|constexpr\\|decltype\\|noexcept\\|nullptr\\|static_assert\\|thread_local\\|override\\|final\\)\\>" . font-lock-keyword-face)
                  ("\\<\\(char[0-9]+_t\\)\\>" . font-lock-keyword-face)
                  ;; PREPROCESSOR_CONSTANT
                  ("\\<[A-Z]+[A-Z_]+\\>" . font-lock-constant-face)
                  ;; hexadecimal numbers
                  ("\\<0[xX][0-9A-Fa-f]+\\>" . font-lock-constant-face)
                  ;; integer/float/scientific numbers
                  ("\\<[\\-+]*[0-9]*\\.?[0-9]+\\([ulUL]+\\|[eE][\\-+]?[0-9]+\\)?\\>" . font-lock-constant-face)
                  ;; user-types (customize!)
                  ("\\<[A-Za-z_]+[A-Za-z_0-9]*_\\(t\\|type\\|ptr\\)\\>" . font-lock-type-face)
                  ("\\<\\(xstring\\|xchar\\)\\>" . font-lock-type-face)
                  ))
             ) t)

;;; Remove scroll-bar
(scroll-bar-mode -1)

;;; force-save
(defun my/force-save-buffer ()
  "Save the buffer even if it's not modified."
  (interactive)
  (set-buffer-modified-p t)
  (save-buffer))

(global-set-key (kbd "C-x C-S-s") 'my/force-save-buffer)

;;; js2-mode
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;;; prefer UTF-8
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

;;; enable up and downcase region
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)

(provide 'init)
;;; init.el ends here
