;;; nhexl-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (or (file-name-directory #$) (car load-path)))

;;;### (autoloads nil "nhexl-mode" "nhexl-mode.el" (22562 5483 0
;;;;;;  0))
;;; Generated autoloads from nhexl-mode.el

(autoload 'nhexl-mode "nhexl-mode" "\
Minor mode to edit files via hex-dump format

\(fn &optional ARG)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; nhexl-mode-autoloads.el ends here
